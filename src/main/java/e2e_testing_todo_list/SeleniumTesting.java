package e2e_testing_todo_list;

import io.github.bonigarcia.wdm.WebDriverManager;
import java.util.concurrent.TimeUnit;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;

/**
 * SeleniumTesting initiator
 */
public class SeleniumTesting {
    static WebDriver driver;

    /**
     * declaration and instantiation of objects/variables
     * @param url url of the site to navigate to
     * @return instantiated driver
     */
    public static WebDriver init(String url){
        WebDriverManager.chromedriver().setup();

        //NOTE: remove headless option to see end to end test running in browser
        ChromeOptions options = new ChromeOptions();
        options.setHeadless(false);
        driver =  new ChromeDriver(options);
        driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
        driver.get(url);
        return driver;
    }

    public static void quit(){
        driver.close();
    }
}