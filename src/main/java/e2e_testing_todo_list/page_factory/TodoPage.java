package e2e_testing_todo_list.page_factory;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.ArrayList;
import java.util.List;

/**
 * PageFactory implementation of PageObjectModel(POM) for IconPage
 * url:http://localhost:3000/search/icons
 */
public class TodoPage {

  WebDriver driver;

  Actions actionsQueue;

  @FindBy(how = How.ID, using = "add-todo")
  WebElement newTodo;

  @FindBy(how = How.CLASS_NAME, using = "destroy")
  //  WebElement removeTodo;
  List<WebElement> removeTodoList;

  @FindBy(how = How.CLASS_NAME, using = "view")
  List<WebElement> todoList;

  @FindBy(how = How.CLASS_NAME, using = "toggle")
  WebElement complete;

  public TodoPage(WebDriver driver) {
    this.driver = driver;

    PageFactory.initElements(driver, this);
    actionsQueue = new Actions(driver);
  }

  /**
   * This function add a todo item to the list
   *
   * @param todoItem - String value of the todo item we want to create
   */
  public void addNewTodo(String todoItem) {

    WebElement new_todo = getNewTodo();
    new_todo.sendKeys(todoItem);
    new_todo.sendKeys(Keys.ENTER);
  }

  /** This function removes the last todo on the list */
  public void removeTodo() {
    WebElement removeTodo = removeTodoList.get(0);
    getActionsQueue()
        .moveToElement(
            driver.findElement(
                By.xpath("//html/body/section/section/div/section/ul/li/div/label")));
    getActionsQueue()
        .moveToElement(
            driver.findElement(
                By.xpath("//html/body/section/section/div/section/ul/li/div/button")))
        .perform();
    removeTodo.click();
    new WebDriverWait(driver, 2)
        .until(
            ExpectedConditions.invisibilityOfElementLocated(
                By.xpath("//html/body/section/section/div/section/ul/li[1]/div/label")));
  }

  public void removeTodoByIndex(int index) {
    WebElement removeTodo = removeTodoList.get(index - 1);
    System.out.println(removeTodo.getText());
    getActionsQueue()
        .moveToElement(
            driver.findElement(
                By.xpath(
                    "//html/body/section/section/div/section/ul/li[" + (index) + "]/div/label")));
    getActionsQueue()
        .moveToElement(
            driver.findElement(
                By.xpath(
                    "//html/body/section/section/div/section/ul/li[" + (index) + "]/div/button")))
        .perform();
    removeTodo.click();
    new WebDriverWait(driver, 2)
        .until(
            ExpectedConditions.invisibilityOfElementLocated(
                By.xpath(
                    "//html/body/section/section/div/section/ul/li[" + (index) + "]/div/label")));
  }

  /** This function removes all todos from list */
  public void emptyTodoList() {
    int listSize = todoList.size() - 1;
    System.out.println(listSize);
    if (!todoList.isEmpty()) {
      for (int i = listSize; i >= 0; i--) {
        removeTodoByIndex(i + 1);
      }
    }
  }

  public void CompleteTodo(String todoItem) {
    getActionsQueue()
        .moveToElement(
            driver.findElement(By.xpath("//html/body/section/section/div/section/ul/li/div/label")))
        .perform();
    complete.click();
  }

  public WebDriver getDriver() {
    return driver;
  }

  public Actions getActionsQueue() {
    return actionsQueue;
  }

  public WebElement getRemoveTodo(int index) {
    return removeTodoList.get(index);
  }

  public WebElement getNewTodo() {
    return newTodo;
  }

  public List<String> getTodoLabelsList() {
    List<String> labelsList = new ArrayList<>();
    for (WebElement webElement : todoList) {
      labelsList.add(webElement.getText());
    }
    return labelsList;
  }
}
