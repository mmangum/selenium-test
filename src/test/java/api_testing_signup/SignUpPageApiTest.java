package api_testing_signup;

import io.restassured.RestAssured;
import org.junit.*;

import java.util.HashMap;
import java.util.Map;

import static io.restassured.RestAssured.*;
import static org.hamcrest.Matchers.*;

public class SignUpPageApiTest {

  @BeforeClass
  public static void setup() {
    String port = System.getProperty("server.port");
    if (port == null) {
      RestAssured.port = 3000;
    } else {
      RestAssured.port = Integer.parseInt(port);
    }

    String basePath = System.getProperty("server.base");
    if (basePath == null) {
      basePath = "/signup";
    }
    RestAssured.basePath = basePath;

    String baseHost = System.getProperty("server.host");
    if (baseHost == null) {
      baseHost = "http://localhost";
    }
    RestAssured.baseURI = baseHost;
  }

  @After
  public void clearAccountsAfter() {
    RestAssured.basePath = "/accounts";
    delete();
  }

  @Before
  public void clearAccounts() {
    RestAssured.basePath = "/accounts";
    delete();
    RestAssured.basePath = "/signup";
  }

  @Test
  public void PostNewSignUp_Expect201AndResponseObject() {

    Map<String, String> signup = new HashMap<>();
    signup.put("email", "johndoe@gmail.com");
    signup.put("password", "thisisagoodpassword");

    with()
        .contentType("application/json")
        .body(signup)
        .when()
        .post()
        .then()
        .statusCode(201)
        .body("email", equalTo("johndoe@gmail.com"))
        .body("password", equalTo("thisisagoodpassword"));

    RestAssured.basePath = "/accounts";
    get()
        .then()
        .statusCode(200)
        .assertThat()
        .body("email", hasItem("johndoe@gmail.com"))
        .body("password", hasItem("thisisagoodpassword"));
  }

  @Test
  public void PostNewSignUpExistingEmail_Expect409AndResponseObject() {

    Map<String, String> signupFirstRequest = new HashMap<>();
    signupFirstRequest.put("email", "johndoe@gmail.com");
    signupFirstRequest.put("password", "thisisagoodpassword");

    with()
        .contentType("application/json")
        .body(signupFirstRequest)
        .when()
        .post()
        .then()
        .statusCode(201);

    Map<String, String> signupSecondRequest = new HashMap<>();
    signupSecondRequest.put("email", "johndoe@gmail.com");
    signupSecondRequest.put("password", "thisisanothergoodpassword");

    with()
        .contentType("application/json")
        .body(signupSecondRequest)
        .when()
        .post()
        .then()
        .statusCode(409)
        .body("error", equalTo("email is already taken"));

    RestAssured.basePath = "/accounts";
    get()
        .then()
        .statusCode(200)
        .assertThat()
        .body("email", hasItem("johndoe@gmail.com"))
        .body("password", hasItem("thisisagoodpassword"))
        .body("password", not("thisisanothergoodpassword"));
  }
  @Test
  public void PostNewSignUpMissingPassword_Expect401AndResponseObject() {

    Map<String, String> signupRequest = new HashMap<>();
    signupRequest.put("email", "mikemango@gmail.com");

    with()
            .contentType("application/json")
            .body(signupRequest)
            .when()
            .post()
            .then()
            .statusCode(401)
            .body("error", equalTo("email and password are required"));

    RestAssured.basePath = "/accounts";
    get()
            .then()
            .statusCode(200)
            .assertThat()
            .body("email", empty())
            .body("password", empty());
  }
}
