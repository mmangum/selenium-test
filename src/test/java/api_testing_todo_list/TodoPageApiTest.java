package api_testing_todo_list;

import io.restassured.RestAssured;

import org.junit.After;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import java.util.HashMap;
import java.util.Map;

import static io.restassured.RestAssured.*;
import static org.hamcrest.Matchers.*;

public class TodoPageApiTest {

  public static void AddTwoItemsToTodoList() {
    Map<String, Object> todo1 = new HashMap<>();
    todo1.put("title", "Play in traffic");
    todo1.put("completed", false);
    todo1.put("id", 7478817887L);

    Map<String, Object> todo2 = new HashMap<>();
    todo2.put("title", "Try not to get run over");
    todo2.put("completed", false);
    todo2.put("id", 8877654321L);

    with().contentType("application/json").body(todo1).when().post().then().statusCode(201);

    with().contentType("application/json").body(todo2).when().post().then().statusCode(201);
  }

  @BeforeClass
  public static void setup() {
    String port = System.getProperty("server.port");
    if (port == null) {
      RestAssured.port = 3000;
    } else {
      RestAssured.port = Integer.parseInt(port);
    }

    String basePath = System.getProperty("server.base");
    if (basePath == null) {
      basePath = "/todos";
    }
    RestAssured.basePath = basePath;

    String baseHost = System.getProperty("server.host");
    if (baseHost == null) {
      baseHost = "http://localhost";
    }
    RestAssured.baseURI = baseHost;


  }

  @Before
  public void DeleteTodoListBefore() {
    delete().then().statusCode(204);
  }

  @After
  public void DeleteTodoListAfter() {

    delete().then().statusCode(204);
  }

  @Test
  public void GetAllTodos_Expect200ResponseAndResponseObject() {

    AddTwoItemsToTodoList();

    get()
        .then()
        .statusCode(200)
        .assertThat()
        .body("title", hasItems("Play in traffic", "Try not to get run over"))
        .body("completed", hasItems(false, false))
        .body("id", hasItems(7478817887L, 8877654321L));


  }

  @Test
  public void GetTodoById_Expect200AndResponseObject() {

    AddTwoItemsToTodoList();

    Long id = 7478817887L;

    get("/" + id.toString())
        .then()
        .statusCode(200)
        .assertThat()
        .body("title", equalTo("Play in traffic"))
        .body("completed", equalTo(false))
        .body("id", equalTo(id));


  }

  @Test
  public void PostNewTodo_Expect201AndResponseObject() {

    Map<String, Object> todo = new HashMap<>();
    todo.put("title", "Try not to get run over");
    todo.put("completed", false);
    todo.put("id", 8877654321L);

    with().contentType("application/json").body(todo).when().post().then().statusCode(201);
    Long id = 8877654321L;
    get(id.toString())
        .then()
        .statusCode(200)
        .assertThat()
        .body("title", equalTo("Try not to get run over"))
        .body("completed", equalTo(false))
        .body("id", equalTo(id));

  }

  @Test
  public void DeleteAllTodos_Expect204() {

    AddTwoItemsToTodoList();

    delete().then().statusCode(204);

    get()
        .then()
        .statusCode(200)
        .assertThat()
        .body("title", empty())
        .body("complete", empty())
        .body("id", empty());
  }
}
