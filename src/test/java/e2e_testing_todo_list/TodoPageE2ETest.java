package e2e_testing_todo_list;

import com.google.common.collect.ImmutableList;
import org.junit.*;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import e2e_testing_todo_list.page_factory.TodoPage;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

import static org.junit.Assert.*;

public class TodoPageE2ETest {

  TodoPage todoPage;

  static WebDriver driver;

  @Before
  public void init() {
    todoPage = new TodoPage(SeleniumTesting.init("http://localhost:3000/"));
    driver = todoPage.getDriver();
    driver.manage().window().maximize();
    todoPage.emptyTodoList();
  }

  @After
  public void closeBrowser() {
    SeleniumTesting.quit();
  }

  @Test
  public void test01_AddTextToTodoListAndVerifyList() {
    String todo = "Finish test #1";
    todoPage.addNewTodo(todo);

    assertEquals(
        todo + " is not found in the list",
        driver.findElement(By.cssSelector(".view")).getText(),
        todo);

    todoPage.removeTodo();
  }

  @Test
  public void test02_HoverOverItemAndDeleteFromList() {
    String todo1 = "Finish test #1";
    String todo2 = "Finish test #2";
    todoPage.addNewTodo(todo2);
    todoPage.addNewTodo(todo1);
    todoPage.removeTodo();
    assertNotEquals(
        todo2 + " is still in the list",
        todo2,
        driver.findElement(By.cssSelector(".view")).getText());
    todoPage.removeTodo();
  }

  @Test
  public void test03_ClickOnCircleItemCrossedOutAndItemGreyedOut() {
    String todo = "Finish test #3";
    todoPage.addNewTodo(todo);
    todoPage.CompleteTodo(todo);
    WebElement complete = driver.findElement(By.className("toggle"));
    WebElement labelText =
        driver.findElement(By.xpath("//html/body/section/section/div/section/ul/li/div/label"));
    assertTrue(complete.isSelected());
    assertTrue(labelText.getCssValue("text-decoration").contains("line-through"));
    driver.manage().timeouts().implicitlyWait(1, TimeUnit.SECONDS);
    todoPage.removeTodo();
  }

  @Test
  public void test04_HoverOverItemAndRemoveItemListCollapsesAndListNotReordered() {
    String todo1 = "Finish test #1";
    String todo2 = "Finish test #2";
    String todo3 = "Finish test #3";
    String todo4 = "Finish test #4";
    List<String> expectedList = ImmutableList.of(todo1, todo3, todo4);
    todoPage.addNewTodo(todo1);
    todoPage.addNewTodo(todo2);
    todoPage.addNewTodo(todo3);
    todoPage.addNewTodo(todo4);
    todoPage.removeTodoByIndex(2);

    List<String> todoList = todoPage.getTodoLabelsList();
    System.out.println(todoList);
    Assert.assertEquals(todoList, expectedList);
    todoPage.emptyTodoList();
  }
}
