package api_testing_login;

import io.restassured.RestAssured;
import org.junit.After;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import java.util.HashMap;
import java.util.Map;

import static io.restassured.RestAssured.*;
import static org.hamcrest.Matchers.*;

public class LoginPageApiTest {
    @BeforeClass
    public static void setup() {
        String port = System.getProperty("server.port");
        if (port == null) {
            RestAssured.port = 3000;
        } else {
            RestAssured.port = Integer.parseInt(port);
        }

        String basePath = System.getProperty("server.base");
        if (basePath == null) {
            basePath = "/login";
        }
        RestAssured.basePath = basePath;

        String baseHost = System.getProperty("server.host");
        if (baseHost == null) {
            baseHost = "http://localhost";
        }
        RestAssured.baseURI = baseHost;


    }
    @After
    public void clearAccountsAfter() {
        RestAssured.basePath = "/accounts";
        delete();
    }

    @Before
    public void clearAccounts() {
        RestAssured.basePath = "/accounts";
        delete();
        RestAssured.basePath = "/login";
    }

    @Test
    public void PostLoginExistingUser_Expect201AndResponseObject() {

        Map<String, String> signup = new HashMap<>();
        signup.put("email", "johndoe@gmail.com");
        signup.put("password", "password");
        RestAssured.basePath = "/signup";
        with()
                .contentType("application/json")
                .body(signup)
                .when()
                .post()
                .then()
                .statusCode(201)
                .body("email", equalTo("johndoe@gmail.com"))
                .body("password", equalTo("password"));

        RestAssured.basePath = "/accounts";
        get()
                .then()
                .statusCode(200)
                .assertThat()
                .body("email", hasItem("johndoe@gmail.com"))
                .body("password", hasItem("password"));

        Map<String, String> login = new HashMap<>();
        login.put("email", "johndoe@gmail.com");
        login.put("password", "password");
        RestAssured.basePath = "/login";
        with()
                .contentType("application/json")
                .body(login)
                .when()
                .post()
                .then()
                .statusCode(200)
                .body("message", equalTo("User is logged in"))
                .body("token", equalTo("true"));


    }

    @Test
    public void PostLoginExistingUserWrongPassword_Expect401AndResponseObject() {

        Map<String, String> signup = new HashMap<>();
        signup.put("email", "johndoe@gmail.com");
        signup.put("password", "password");
        RestAssured.basePath = "/signup";
        with()
                .contentType("application/json")
                .body(signup)
                .when()
                .post()
                .then()
                .statusCode(201)
                .body("email", equalTo("johndoe@gmail.com"))
                .body("password", equalTo("password"));

        RestAssured.basePath = "/accounts";
        get()
                .then()
                .statusCode(200)
                .assertThat()
                .body("email", hasItem("johndoe@gmail.com"))
                .body("password", hasItem("password"));

        Map<String, String> login = new HashMap<>();
        login.put("email", "johndoe@gmail.com");
        login.put("password", "passwordnotcorrect");
        RestAssured.basePath = "/login";
        with()
                .contentType("application/json")
                .body(login)
                .when()
                .post()
                .then()
                .statusCode(401)
                .body(containsString("Unauthorized"));


    }
}
